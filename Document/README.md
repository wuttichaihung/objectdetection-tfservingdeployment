# Object Detection TensorFlow Serving Deployment

<pre class="prettyprint lang-bsh">
# Download or clone the project
1. "https://drive.google.com/drive/folders/1bkz3PF_7_fP_q2O7NksqxDLDC-lyTegS?usp=sharing"
or "git clone https://gitlab.com/wuttichaihung/objectdetection-tfservingdeployment"

2. "cd objectDetection-TFservingDeployment"

# Download a TensorFlow Serving Docker image
3. "docker pull tensorflow/serving"

# Create and run a TensorFlow Serving docker containing the model and open the REST API port
4. "docker run -t --rm -d -p 8501:8501 -v "$(pwd)/saved_model:/models/object_detection" -e MODEL_NAME=object_detection tensorflow/serving"

# Install dependencies
5. "pip install -r requirements.txt"

# Run exmaple code using the external Tensorflow Serving model
6. "python test_request.py"
