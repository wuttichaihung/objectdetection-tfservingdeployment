import tensorflow as tf
from tensorflow.python.saved_model import signature_constants
from tensorflow.python.saved_model import tag_constants

model_version = 1 
export_dir = './saved_model/' + str(model_version)
graph_pb = 'detector_frozen.pb'

builder = tf.saved_model.builder.SavedModelBuilder(export_dir)

with tf.gfile.GFile(graph_pb, "rb") as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())

sigs = {}

with tf.Session(graph=tf.Graph()) as sess:
    # name="" is important to ensure we don't get spurious prefixing
    tf.import_graph_def(graph_def, name="")
    g = tf.get_default_graph()
    input_data = g.get_tensor_by_name("input/input_data:0")
    pred_lbbox = g.get_tensor_by_name("pred_lbbox/concat_2:0")
    pred_sbbox = g.get_tensor_by_name("pred_sbbox/concat_2:0")
    pred_mbbox = g.get_tensor_by_name("pred_mbbox/concat_2:0")

    sigs[signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY] = \
        tf.saved_model.signature_def_utils.predict_signature_def(
            {"input_data": input_data}, {"pred_lbbox": pred_lbbox, "pred_sbbox": pred_sbbox, "pred_mbbox":pred_mbbox})

    builder.add_meta_graph_and_variables(sess,
                                         [tag_constants.SERVING],
                                         signature_def_map=sigs)

builder.save()