import requests
import time
import cv2
import numpy as np
from PIL import Image
from library import image_preprocess, postprocess_boxes, nms, draw_bbox

num_classes = 80
input_size = 416
original_image = cv2.imread("images/road.jpeg")
original_image = cv2.cvtColor(original_image, cv2.COLOR_BGR2RGB)
original_image_size = original_image.shape[:2]
image_data = image_preprocess(np.copy(original_image), [input_size, input_size])
image_data = image_data[np.newaxis, ...]


path_url = "http://localhost:8501/v1/models/object_detection:predict"
payload = {"instances": image_data.tolist()}
start = time.time()
res = requests.post(path_url, json=payload)
print("duration :" + str(round(time.time()-start, 2)))
res_dict = res.json()['predictions'][0]
pred_sbbox = res_dict['pred_sbbox']
pred_lbbox = res_dict['pred_lbbox']
pred_mbbox = res_dict['pred_mbbox']


pred_bbox = np.concatenate([np.reshape(pred_sbbox, (-1, 5 + num_classes)),
                                np.reshape(pred_mbbox, (-1, 5 + num_classes)),
                                np.reshape(pred_lbbox, (-1, 5 + num_classes))], axis=0)
bboxes = postprocess_boxes(pred_bbox, original_image_size, input_size, 0.3)
bboxes = nms(bboxes, 0.45, method='nms')
image = draw_bbox(original_image, bboxes)
image = Image.fromarray(image)
image.save("images/output.jpg")
image.show()
